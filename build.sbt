name := "typesafe-config-bug"
scalaVersion := "2.13.6"

libraryDependencies += "com.typesafe" % "config" % "1.4.1"
libraryDependencies += "com.github.pureconfig" %% "pureconfig" % "0.16.0"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.9" % Test

Test / javaOptions ++= Seq("-Dconfig.override_with_env_vars=true")
Test / fork := true
Test / envVars := Map(
  "CONFIG_FORCE_items_0" -> "override0",
  "CONFIG_FORCE_items_1" -> "override1",
)
