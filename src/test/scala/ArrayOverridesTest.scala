import org.scalatest.EitherValues
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers
import pureconfig.ConfigSource

class ArrayOverridesTest extends AnyFreeSpec with Matchers with EitherValues {

  "array override should work" in {
    val items = ConfigSource.default.at("items").load[List[String]]
    items.value should contain theSameElementsInOrderAs List("override0", "override1")
  }
}
